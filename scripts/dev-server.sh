set -o errexit -o nounset

cargo fmt
cargo test

wasm-pack build --dev --no-typescript --target no-modules --out-dir static/target --out-name wasm crates/frontend-cdylib

RUST_BACKTRACE=1 cargo run --profile dev --package workspace-backend --features "static-files"
