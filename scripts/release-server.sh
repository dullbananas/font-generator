set -o errexit -o nounset

#cargo fmt

#wasm-pack build --release --no-typescript --target no-modules --out-dir static/target --out-name wasm crates/frontend-cdylib -- --features "full"

cargo run --profile release --package workspace-backend --features "full"
