set -o errexit -o nounset

DIR=font-generator-deployment

wasm-pack build --release --no-typescript --target no-modules --out-dir static/target --out-name wasm crates/frontend-cdylib -- --features "full"

cargo build --profile release --package workspace-backend --features "full" --target $TARGET

rm -rf $DIR/target
cp crates/frontend-cdylib/static/* -t $DIR --force -r
rm $DIR/target/.gitignore
cp index.html $DIR/index.html
cp index.html $DIR/editor.html
cp index.html $DIR/test.html

rm --force $DIR/target/backend
mv target/$TARGET/release/workspace-backend $DIR/target/backend

git log -1 > $DIR/commit.txt
