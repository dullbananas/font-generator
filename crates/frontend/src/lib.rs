use seed::{prelude::*, *};
use workspace_font_editor as font_editor;
use workspace_font_test as font_test;

fn init(
    mut url: Url,
    orders: &mut impl Orders<Msg>,
) -> Model {
    Model {
        page: match url.next_path_part() {
            Some("editor.html") => {
                Page::FontEditor(font_editor::init(
                    url,
                    &mut orders.proxy(Msg::FontEditor),
                ))
            }
            Some("test.html") => {
                Page::FontTest(font_test::init(
                    url,
                    &mut orders.proxy(Msg::FontTest),
                ))
            }
            _ => Page::NotFound,
        },
    }
}

struct Model {
    page: Page,
}

enum Page {
    FontEditor(font_editor::Model),
    FontTest(font_test::Model),
    NotFound,
}

enum Msg {
    FontEditor(font_editor::Msg),
    FontTest(font_test::Msg),
}

fn update(
    msg: Msg,
    model: &mut Model,
    orders: &mut impl Orders<Msg>,
) {
    match (msg, &mut model.page) {
        (Msg::FontEditor(msg), Page::FontEditor(model)) => {
            let _ = font_editor::update(
                msg,
                model,
                &mut orders.proxy(Msg::FontEditor),
            );
        }
        (Msg::FontTest(msg), Page::FontTest(model)) => {
            let _ = font_test::update(
                msg,
                model,
                &mut orders.proxy(Msg::FontTest),
            );
        }
        _ => {}
    }
}

fn view(model: &Model) -> Node<Msg> {
    div![
        C!["col", "fill"],
        id! {"app"},
        match &model.page {
            Page::FontEditor(model) => {
                font_editor::view(model)
                    .map_msg(Msg::FontEditor)
            }
            Page::FontTest(model) => {
                font_test::view(model)
                    .map_msg(Msg::FontTest)
            }
            Page::NotFound => {
                nodes![div![
                    C!["box", "fill"],
                    "The requested page does not exist.",
                ],]
            }
        },
    ]
}

pub fn start() {
    std::panic::set_hook(Box::new(
        console_error_panic_hook::hook,
    ));

    seed::app::App::start("app", init, update, view);
}
