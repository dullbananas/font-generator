use seed::{prelude::*, *};
use std::borrow::Borrow;
use workspace_glyph::{Glyph, Metrics, Segment};

/// A `Glyph` is displayed by placing this inside of an `svg` element. It will fill the parent element's entire width or height.
#[derive(Clone)]
pub struct GlyphSvg<T> {
    glyph: T,
    metrics: Metrics,
    path_d: Vec<u8>,
}

impl<T: Borrow<Glyph>> GlyphSvg<T> {
    pub fn new(glyph: T, metrics: Metrics) -> Self {
        let path_d = Vec::with_capacity(
            capacity(glyph.borrow()) * 2,
        );
        let mut result = GlyphSvg {
            glyph,
            metrics,
            path_d,
        };
        result.update_path_d();

        result
    }

    pub fn set_glyph(&mut self, glyph: T) {
        self.glyph = glyph;
        self.update_path_d();
    }

    pub fn take_glyph(&mut self) -> T
    where
        T: Default,
    {
        self.path_d.clear();
        std::mem::take(&mut self.glyph)
    }

    pub fn view(&self) -> GlyphSvgView<'_, T> {
        GlyphSvgView(self)
    }

    pub fn set_metrics(&mut self, metrics: Metrics) {
        self.metrics = metrics;
    }

    fn update_path_d(&mut self) {
        self.path_d.clear();
        self.path_d.reserve(capacity(self.glyph.borrow()));
        for path in &self.glyph.borrow().paths {
            // 5 digits prefixed with + or -
            let [x, y] = path.first_point;
            self.path_d.extend_from_slice(
                format!("M{x:+06}{y:+06}").as_bytes(),
            );
            for &Segment([x, y]) in &path.directions {
                self.path_d.extend_from_slice(
                    format!("l{x:+06}{y:+06}").as_bytes(),
                );
            }
        }
    }
}

fn capacity(glyph: &Glyph) -> usize {
    glyph
        .paths
        .iter()
        .map(|path| (path.directions.len() * 13) + 13)
        .sum()
}

/// `UpdateEl` can't be implemented directly on `&GlyphSvgView` because it conflicts with a built-in implementation in Seed, which would clone the `GlyphSvg` value.
pub struct GlyphSvgView<'a, T>(&'a GlyphSvg<T>);

impl<'a, T: Borrow<Glyph>, Ms> UpdateEl<Ms>
    for GlyphSvgView<'a, T>
{
    fn update_el(self, el: &mut El<Ms>) {
        attrs! {
            At::Xmlns => "http://www.w3.org/2000/svg",
            At::ViewBox => {
                let rect = self.0.metrics.rect;
                let axgeom::Vec2 { x, y } = rect.top_left();
                let width = rect.x.end.abs_diff(rect.x.start);
                let height = rect.y.end.abs_diff(rect.y.start);
                let side = std::cmp::max(width, height);
                format!("{x} {y} {side} {side}")
            },
        }
        .update_el(el);

        nodes![path![attrs! {
            At::FillRule => "nonzero",
            At::D => std::str::from_utf8(&self.0.path_d).unwrap(),
        },],]
        .update_el(el);
    }
}
