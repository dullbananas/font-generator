mod source_font;

use crate::source_font::SourceFont;
use api::{Password, ResponseMsg};
use js_sys::Uint8Array;
use seed::{prelude::*, *};
use std::{borrow::Cow, collections::BTreeMap};
use web_sys::HtmlInputElement;
use workspace_data as api;
use workspace_frontend_shared::{
    char_input, server_request,
};
use workspace_glyph::{GlyphItem, Metrics};
use workspace_glyph_builder::GlyphBuilder;
use workspace_glyph_svg::GlyphSvg;

pub fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model {
        selected_glyph: None,
        source_font: None,
        status: Cow::Borrowed(""),
        password: String::new(),
        el_refs: Default::default(),
    }
}

pub struct Model {
    selected_glyph: Option<SelectedGlyph>,
    source_font: Option<SourceFont>,
    status: Cow<'static, str>,
    password: String,
    el_refs: ElRefs,
}

struct SelectedGlyph {
    svg: GlyphSvg<GlyphBuilder>,
}

#[derive(Default)]
struct ElRefs {
    file_input: ElRef<HtmlInputElement>,
}

pub enum Msg {
    Import,
    FontReadDone(Result<JsValue, JsValue>),
    ToggleMutate,
    CharInput(char),
    SetPassword(String),
    Upload,
    ServerMsg(Result<api::ResponseMsg, String>),
}

pub fn update(
    msg: Msg,
    model: &mut Model,
    orders: &mut impl Orders<Msg>,
) -> Option<()> {
    match msg {
        Msg::Import => {
            model.status = Cow::Borrowed("Importing font");
            let file = model
                .el_refs
                .file_input
                .get()?
                .files()?
                .item(0)?;
            orders.perform_cmd(async move {
                Msg::FontReadDone(
                    JsFuture::from(file.array_buffer())
                        .await,
                )
            });
        }
        Msg::FontReadDone(Ok(array_buffer)) => {
            let data =
                Uint8Array::new(&array_buffer).to_vec();
            match SourceFont::parse(data) {
                Ok(source_font) => {
                    model.source_font = Some(source_font);
                    model.status = Cow::Borrowed("");
                }
                Err(error) => {
                    model.status = error;
                }
            }
        }
        Msg::FontReadDone(Err(_)) => {
            model.status =
                Cow::Borrowed("Failed to read file")
        }
        Msg::ToggleMutate => {
            if let Some(SelectedGlyph { svg, .. }) =
                &mut model.selected_glyph
            {
                let mut builder = svg.take_glyph();
                builder.swap();
                svg.set_glyph(builder);
            }
        }
        Msg::CharInput(char) => {
            let font = model.source_font.as_ref()?;
            let glyph = font.get_glyph(char)?;
            let mut builder =
                GlyphBuilder::from_glyph(glyph);
            builder.mutate_child().unwrap();
            let metrics = Metrics::from_glyphs([
                builder.child_glyph()
            ]);
            model.selected_glyph = Some(SelectedGlyph {
                svg: GlyphSvg::new(builder, metrics),
            });
        }
        Msg::SetPassword(s) => {
            model.password = s;
        }
        Msg::Upload => {
            let Ok(password) = model.password.parse::<Password>() else {
                model.status = Cow::Borrowed("Wrong password");
                return Some(());
            };
            let font = model.source_font.as_ref()?;
            let mut mutation_hashes = BTreeMap::new();
            let glyphs = ('\u{0}'..char::MAX)
                .filter_map(|char| {
                    let glyph = font.get_glyph(char)?;
                    if char == 'a' {
                        let mut glyph_builder =
                            GlyphBuilder::from_glyph(
                                glyph.clone(),
                            );
                        let mutation = glyph_builder
                            .mutate_child()
                            .unwrap();
                        mutation_hashes.insert(
                            char,
                            (
                                glyph_builder
                                    .child
                                    .to_hash(),
                                mutation,
                            ),
                        );
                    }
                    Some(GlyphItem { char, glyph })
                })
                .collect::<Vec<_>>();
            let license = font.license().into_bytes();
            server_request(
                orders,
                Msg::ServerMsg,
                api::RequestMsg::UploadGlyphs(Box::new(
                    api::UploadGlyphs {
                        password,
                        license,
                        glyphs,
                        mutation_hashes,
                    },
                )),
            );
        }
        Msg::ServerMsg(Ok(msg)) => match msg {
            ResponseMsg::FontUploaded { font_id } => {
                model.status = Cow::Borrowed("");
                Url::new()
                    .add_path_part("test.html")
                    .set_search(UrlSearch::new([(
                        "font",
                        [font_id.to_string()],
                    )]))
                    .go_and_load();
            }
            ResponseMsg::WrongPassword => {
                model.status =
                    Cow::Borrowed("Wrong password");
            }
            _ => {}
        },
        Msg::ServerMsg(Err(err)) => {
            model.status = Cow::Owned(err);
        }
    }
    Some(())
}

pub fn view(model: &Model) -> Vec<Node<Msg>> {
    nodes![
        div![
            C!["box"],
            char_input::view(Msg::CharInput),
            button![
                ev(Ev::Click, |_| Msg::ToggleMutate),
                "Toggle mutation",
            ],
            label![input![
                attrs! {
                    At::Type => "file",
                    At::Accept => ".dat",
                },
                input_ev(Ev::Input, |_| Msg::Import),
                el_ref(&model.el_refs.file_input),
            ],],
            input![
                attrs! {
                    At::Type=>"text",
                },
                input_ev(Ev::Input, Msg::SetPassword),
            ],
            button![
                ev(Ev::Click, |_| Msg::Upload),
                "Upload",
            ],
            textarea![model
                .source_font
                .as_ref()
                .map(|f| f.license())
                .unwrap_or_else(String::new)],
            model.status,
            "\u{00A0}",
        ],
        div![
            C!["row", "fill"],
            div![
                C!["col", "fill"],
                match model.selected_glyph {
                    Some(ref selected_glyph) =>
                        view_glyph_editor(selected_glyph),
                    None => nodes![div![C!["box", "fill"]]],
                },
            ],
        ],
    ]
}

fn view_glyph_editor(
    selected_glyph: &SelectedGlyph,
) -> Vec<Node<Msg>> {
    nodes![div![
        C!["glyph-large"],
        svg![
            C!["glyph-large-inner"],
            selected_glyph.svg.view(),
        ],
    ],]
}
