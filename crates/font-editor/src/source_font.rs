use lyon_geom::{
    CubicBezierSegment, Point, QuadraticBezierSegment,
};
use owned_ttf_parser::{
    AsFaceRef, Face, FaceParsingError, OutlineBuilder,
    OwnedFace,
};
use std::borrow::Cow;
use workspace_glyph::{points_op, Glyph, Path, Segment};

const MAX_COMPONENT: i16 = 24576;

pub struct SourceFont {
    face: OwnedFace,
}

impl SourceFont {
    pub fn parse(
        data: Vec<u8>,
    ) -> Result<Self, Cow<'static, str>> {
        let face = OwnedFace::from_vec(data, 0)
            .map_err(format_parse_error)?;

        Ok(SourceFont { face })
    }

    pub fn license(&self) -> String {
        let names = self
            .face
            .as_face_ref()
            .names()
            .into_iter()
            .map(|n| (n.name_id, n.to_string()))
            .collect::<Vec<_>>();
        let get_name = |index, default: &str| {
            names
                .iter()
                .find_map(|(id, s)| {
                    s.clone().filter(|_| *id == index)
                })
                .unwrap_or_else(|| default.to_owned())
        };
        let notice = get_name(0, "");
        let name = get_name(4, "");
        let license_desc = get_name(13, "");
        format!("This font is a modified version of {name}.\n\nLicense of {name}:\n\n{notice}\n\n{license_desc}")
        /*format!(
            "{:?}",
            names
                .into_iter()
                .map(|n| (n.name_id, n.to_string()))
                .collect::<Vec<_>>()
        )*/
    }

    pub fn get_glyph(&self, char: char) -> Option<Glyph> {
        let face = self.face.as_face_ref();
        let mut builder = SegmentCollector {
            glyph: Glyph { paths: Vec::new() },
            next_path: Path {
                first_point: [0, 0],
                directions: Vec::new(),
            },
            position: [0, 0],
            next_segment_start: Point::new(0.0, 0.0),
            scale: get_scale(face),
        };
        let glyph_id = face.glyph_index(char)?;
        face.outline_glyph(glyph_id, &mut builder);
        Some(builder.glyph)
    }
}

fn format_parse_error(
    error: FaceParsingError,
) -> Cow<'static, str> {
    Cow::Borrowed(match error {
        FaceParsingError::FaceIndexOutOfBounds => "Imported file is an empty font collection",
        _ => "Imported file is not a valid TrueType, OpenType, or AAT font",
    })
}

fn get_scale(face: &Face) -> f32 {
    let r = face.global_bounding_box();
    f32::from(MAX_COMPONENT)
        / f32::from(
            [r.x_min, r.y_min, r.x_max, r.y_max]
                .into_iter()
                .max()
                .unwrap(),
        )
}

struct SegmentCollector {
    glyph: Glyph,
    next_path: Path,
    position: [i16; 2],
    next_segment_start: Point<f32>,
    scale: f32,
}

impl SegmentCollector {
    fn tolerance(&self) -> f32 {
        0.5
    }

    fn point(&self, x: f32, y: f32) -> Point<f32> {
        [x, y].map(|n| n * self.scale).into()
    }

    fn convert_point(&self, point: [f32; 2]) -> [i16; 2] {
        point.map(|n| n.floor() as i16)
    }

    fn trace(
        &mut self,
        points: impl Iterator<Item = Point<f32>>,
    ) {
        for point in points {
            let end = points_op(
                std::ops::Sub::sub,
                self.convert_point(point.into()),
                self.position,
            );
            self.next_path.directions.extend(
                Segment::from_outside_point(end).map(
                    |direction| {
                        self.position = points_op(
                            std::ops::Add::add,
                            self.position,
                            direction.to_point(),
                        );
                        direction
                    },
                ),
            );
        }
    }
}

impl OutlineBuilder for SegmentCollector {
    fn move_to(&mut self, x: f32, y: f32) {
        let point =
            self.convert_point(self.point(x, y).into());
        self.next_path.first_point = point;
        self.position = point;
        self.next_segment_start = self.point(x, y);
    }

    fn line_to(&mut self, x: f32, y: f32) {
        let end = self.point(x, y);
        self.trace(std::iter::once(end));
        self.next_segment_start = end;
    }

    fn quad_to(
        &mut self,
        x1: f32,
        y1: f32,
        x: f32,
        y: f32,
    ) {
        let end = self.point(x, y);
        let curve = QuadraticBezierSegment {
            from: self.next_segment_start,
            ctrl: self.point(x1, y1),
            to: end,
        };
        self.next_segment_start = end;
        self.trace(curve.flattened(self.tolerance()));
    }

    fn curve_to(
        &mut self,
        x1: f32,
        y1: f32,
        x2: f32,
        y2: f32,
        x: f32,
        y: f32,
    ) {
        let end = self.point(x, y);
        let curve = CubicBezierSegment {
            from: self.next_segment_start,
            ctrl1: self.point(x1, y1),
            ctrl2: self.point(x2, y2),
            to: end,
        };
        self.next_segment_start = end;
        self.trace(curve.flattened(self.tolerance()));
    }

    fn close(&mut self) {
        self.next_path.close();
        self.glyph.paths.push(std::mem::replace(
            &mut self.next_path,
            Path {
                first_point: [0, 0],
                directions: Vec::new(),
            },
        ));
    }
}
