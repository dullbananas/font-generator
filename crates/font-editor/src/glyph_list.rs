use seed::{prelude::*, *};

pub enum Msg<T> {
    SelectGlyph(T),
}

pub fn view<T: Eq + Copy + 'static>(
    glyphs: impl Iterator<Item = (T, char)>,
    selected_id: Option<T>,
) -> Node<Msg<T>> {
    div![
        C!["col", "box", "scroll"],
        h2!["Glyphs"],
        glyphs.map(|(id, char)| {
            div![
                el_key(&char),
                C!["row", "gap", IF!(Some(id) == selected_id => "selected")],
                ev(Ev::Click, move |_| Msg::SelectGlyph(id)),
                char.to_string(),
            ]
        }),
    ]
}
