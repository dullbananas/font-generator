use api::{
    font::{Font, Origin, Score},
    GlyphCandidate, ParentGlyph, RequestMsg, ResponseMsg,
};
use fastrand::Rng;
use js_sys::Date;
use lru::LruCache;
use seed::{prelude::*, *};
use std::{
    borrow::Cow, collections::BTreeMap, num::NonZeroU16,
};
use workspace_data as api;
use workspace_data::database::Id;
use workspace_frontend_shared::{
    char_input, server_request,
};
use workspace_glyph::{Glyph, Metrics};
use workspace_glyph_builder::GlyphBuilder;
use workspace_glyph_svg::GlyphSvg;

pub fn init(
    url: Url,
    orders: &mut impl Orders<Msg>,
) -> Model {
    let mut model = Model {
        font_id: None,
        license: String::new(),
        metrics: Metrics::default(),
        first_glyphs_metrics: Metrics::default(),
        test_items: Vec::new(),
        glyph_data: BTreeMap::new(),
        glyph_cache: LruCache::new(100.try_into().unwrap()),
        start_time: 0.0,
        glyph_visible: false,
        rng: Rng::with_seed(Date::now() as u64),
        status: Cow::Borrowed(""),
    };
    if let Some(font_id) = get_font_id(&url) {
        model.font_id = Some(font_id);
        server_request(
            orders,
            Msg::ServerMsg,
            RequestMsg::StartFirstTest { font_id },
        );
    } else {
        model.status = Cow::Borrowed("Invalid URL");
    }
    model
}

fn get_font_id(url: &Url) -> Option<Id<Font>> {
    url.search().get("font")?.first()?.parse().ok()
}

pub struct Model {
    font_id: Option<Id<Font>>,
    license: String,
    metrics: Metrics,
    first_glyphs_metrics: Metrics,
    test_items: Vec<TestItem>,
    glyph_data: BTreeMap<char, GlyphData>,
    glyph_cache: LruCache<Id<Glyph>, Glyph>,
    start_time: f64,
    glyph_visible: bool,
    rng: Rng,
    status: Cow<'static, str>,
}

impl Model {
    fn cache_glyph(
        &mut self,
        glyph_id: Id<Glyph>,
        glyph: Glyph,
    ) {
        let previous_value =
            self.glyph_cache.put(glyph_id, glyph);
        assert!(previous_value.is_none());
    }
}

struct GlyphData {
    first_glyph: GlyphSvg<Glyph>,
    /// Set to None when this glyph isn't part of the current test
    tested_glyph_data: Option<TestedGlyphData>,
}

struct TestedGlyphData {
    experimental_milliseconds: Vec<NonZeroU16>,
    control_milliseconds: Vec<NonZeroU16>,
    mutated_glyph_id: Id<Glyph>,
    origin: Origin,
}

struct TestItem {
    char: char,
    mutated_glyph: Option<GlyphSvg<Glyph>>,
}

pub enum Msg {
    CharInput(char),
    ShowLicense,
    ServerMsg(Result<api::ResponseMsg, String>),
}

pub fn update(
    msg: Msg,
    model: &mut Model,
    orders: &mut impl Orders<Msg>,
) -> Option<()> {
    match msg {
        Msg::CharInput(input_char) => {
            let test_item = model.test_items.last()?;
            let glyph_data = model
                .glyph_data
                .get_mut(&test_item.char)?;
            let tested_glyph_data =
                glyph_data.tested_glyph_data.as_mut()?;
            if input_char == ' ' {
                model.glyph_visible = true;
                model.start_time = Date::now();
            } else if test_item.char == input_char {
                let milliseconds = (Date::now()
                    - model.start_time)
                    .clamp(0.0, u16::MAX.into())
                    as u16;
                let milliseconds = milliseconds
                    .try_into()
                    .or(1.try_into())
                    .unwrap();
                if test_item.mutated_glyph.is_some() {
                    tested_glyph_data
                        .experimental_milliseconds
                        .push(milliseconds);
                } else {
                    tested_glyph_data
                        .control_milliseconds
                        .push(milliseconds);
                }

                model.test_items.pop();
                model.glyph_visible = false;

                if model.test_items.is_empty() {
                    // Send test results
                    let scores = model
                        .glyph_data
                        .iter_mut()
                        .filter_map(
                            |(&char, glyph_data)| {
                                let TestedGlyphData {
                                    mut experimental_milliseconds,
                                    mut control_milliseconds,
                                    mutated_glyph_id,
                                    origin,
                                } = glyph_data
                                    .tested_glyph_data
                                    .take()?;
                                for vec in [&mut experimental_milliseconds, &mut control_milliseconds] {
                                    assert_eq!(vec.len(), 3);
                                    vec.sort_unstable();
                                }
                                let score = Score {
                                    experimental_milliseconds: *experimental_milliseconds.get(1).unwrap(),
                                    control_milliseconds: *control_milliseconds.get(1).unwrap(),
                                };
                                Some((
                                    GlyphCandidate {
                                        char,
                                        mutated_glyph_id,
                                        origin,
                                    },
                                    score,
                                ))
                            },
                        )
                        .collect::<Vec<_>>();
                    let cached_glyph_ids = model
                        .glyph_cache
                        .iter()
                        .map(|(k, _v)| *k)
                        .collect::<Vec<_>>();
                    server_request(
                        orders,
                        Msg::ServerMsg,
                        RequestMsg::TestDone {
                            font_id: model.font_id.unwrap(),
                            scores,
                            cached_glyph_ids,
                        },
                    );
                }
            } else {
                // Continue waiting for correct char
            }
        }
        Msg::ShowLicense => {
            let _: Result<(), _> =
                window().alert_with_message(&model.license);
        }
        Msg::ServerMsg(Ok(ResponseMsg::TestStarted {
            mutated_glyphs,
            mut metrics,
            init_data,
        })) => {
            if let Some(api::InitData {
                first_glyphs,
                license,
            }) = init_data
            {
                model.first_glyphs_metrics =
                    Metrics::from_glyphs(
                        first_glyphs
                            .iter()
                            .map(|i| &i.glyph),
                    );
                metrics.rect.grow_to_fit(
                    &model.first_glyphs_metrics.rect,
                );
                model.glyph_data = first_glyphs
                    .into_iter()
                    .map(|glyph_item| {
                        (
                            glyph_item.char,
                            GlyphData {
                                first_glyph: GlyphSvg::new(
                                    glyph_item.glyph,
                                    metrics,
                                ),
                                tested_glyph_data: None,
                            },
                        )
                    })
                    .collect();
                model.license = license;
            } else {
                // Update metrics of each GlyphSvg for first glyphs
                // TODO: do this more cleanly
                metrics.rect.grow_to_fit(
                    &model.first_glyphs_metrics.rect,
                );
                for glyph_data in
                    model.glyph_data.values_mut()
                {
                    glyph_data
                        .first_glyph
                        .set_metrics(metrics);
                }
            }

            model.metrics = metrics;

            for (
                GlyphCandidate {
                    char,
                    mutated_glyph_id,
                    origin,
                },
                parent_glyph,
            ) in mutated_glyphs
            {
                let glyph_data =
                    model.glyph_data.get_mut(&char)?;
                glyph_data.tested_glyph_data =
                    Some(TestedGlyphData {
                        experimental_milliseconds:
                            Vec::with_capacity(3),
                        control_milliseconds:
                            Vec::with_capacity(3),
                        mutated_glyph_id,
                        origin: origin.clone(),
                    });
                let parent_glyph = match parent_glyph {
                    ParentGlyph::Cached(glyph_id, hash) => {
                        let glyph = model
                            .glyph_cache
                            .get(&glyph_id)
                            .unwrap()
                            .clone();
                        glyph.check_hash(hash);
                        glyph
                    }
                    ParentGlyph::New(glyph_id, glyph) => {
                        model.cache_glyph(
                            glyph_id,
                            glyph.clone(),
                        );
                        glyph
                    }
                };
                let mut glyph_builder =
                    GlyphBuilder::from_glyph(parent_glyph);
                glyph_builder
                    .apply_mutation(&origin.mutation)
                    .unwrap();
                model.cache_glyph(
                    mutated_glyph_id,
                    glyph_builder.child.clone(),
                );
                model.test_items.extend(
                    std::iter::repeat_with(|| {
                        [
                            TestItem {
                                char,
                                mutated_glyph: Some(
                                    GlyphSvg::new(
                                        glyph_builder
                                            .child
                                            .clone(),
                                        metrics,
                                    ),
                                ),
                            },
                            TestItem {
                                char,
                                mutated_glyph: None,
                            },
                        ]
                    })
                    .take(3)
                    .flatten(),
                );
            }
            model.rng.shuffle(&mut model.test_items);
        }

        Msg::ServerMsg(Ok(_)) => {}
        Msg::ServerMsg(Err(err)) => {
            model.status = Cow::Owned(err);
        }
    }
    Some(())
}

pub fn view(model: &Model) -> Vec<Node<Msg>> {
    nodes![div![
        C!["col", "fill"],
        div![
            C!["box", "test-glyph"],
            current_glyph(model).map(|glyph| svg![
                C!["test-glyph-inner"],
                style!{
                    St::Opacity => if model.glyph_visible { 1 } else { 0 },
                },
                glyph.view()
            ]),
        ],
        div![
            C!["box"],
            char_input::view(Msg::CharInput),
            " ",
            if model.test_items.is_empty() && model.status.is_empty() { "Loading" } else { &model.status },
        ],
        div![
            C!["box"],
            p![format!("Press space in the text field, then type the displayed character as fast as possible. If you type the wrong one, then it will continue waiting until you type the correct one. Repeat this process at least {} times, otherwise no data will be sumbitted.", workspace_data::CHARS_PER_TEST * 2 * 3)],
            button![
                ev(Ev::Click, |_| Msg::ShowLicense),
                "View font license",
            ],
            p![
                a![
                    attrs! {
                        At::Href => "https://codeberg.org/dullbananas/font-generator/src/branch/main",
                    },
                    "Source code",
                ],
            ],
        ],
        div![
            C!["box", "fill"],
        ],
    ],]
}

fn current_glyph(
    model: &Model,
) -> Option<&GlyphSvg<Glyph>> {
    let test_item = model.test_items.last()?;
    test_item.mutated_glyph.as_ref().or_else(|| {
        Some(
            &model
                .glyph_data
                .get(&test_item.char)?
                .first_glyph,
        )
    })
}
