use seed::{prelude::*, *};

pub fn view<Ms: 'static>(
    msg: impl FnOnce(char) -> Ms + Clone + 'static,
) -> Node<Ms> {
    input![
        C!["char-input"],
        attrs! {
            At::Type => "text",
            At::Value => "",
        },
        input_ev(Ev::Input, |string| string
            .chars()
            .next()
            .map(msg)),
    ]
}
