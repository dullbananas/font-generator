pub mod char_input;

use anyhow::Context;
use api::Response;
use reqwest::Client;
use seed::prelude::Orders;
use workspace_data as api;
use workspace_data::database::bincode_config;

pub fn server_request<Ms: 'static>(
    orders: &mut impl Orders<Ms>,
    f: impl FnOnce(Result<api::ResponseMsg, String>) -> Ms
        + 'static,
    msg: api::RequestMsg,
) {
    let url = if let Some(env_url) = api::api_url() {
        env_url.to_owned()
    } else {
        seed::window().location().origin().unwrap() + "/"
    };
    let get_result = async move {
        let client = Client::builder().build()?;
        let body = bincode::encode_to_vec(
            api::Request {
                version: api::CurrentVersion,
                msg,
            },
            bincode_config(),
        )?;
        web_log::println!(
            "Sending {} bytes to {}",
            body.len(),
            url
        );
        let http_response =
            client.post(url).body(body).send().await?;
        let status = http_response.status();
        let bytes = http_response.bytes().await?;
        web_log::println!("Received {} bytes", bytes.len());
        if !status.is_success() {
            anyhow::bail!(
                "Got error response: {}",
                String::from_utf8_lossy(&bytes)
            );
        }
        let response: api::Response =
            bincode::decode_from_slice(
                &bytes,
                bincode_config(),
            )
            .context("Failed to parse server response")?
            .0;
        Ok::<_, anyhow::Error>(response)
    };
    orders.perform_cmd(async move {
        f(match get_result.await {
            Ok(Response::Ok(server_msg)) => Ok(server_msg),
            Ok(Response::WrongVersion) => {
                Err("Please refresh the page".to_owned())
            }
            Ok(Response::Other) => {
                Err("Unexpected error occured on server"
                    .to_owned())
            }
            Err(err) => Err(format!("{err:?}")),
        })
    });
}
