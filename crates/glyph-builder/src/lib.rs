use bincode::{Decode, Encode};
use fastrand::Rng;
use workspace_glyph::{
    points_op, try_points_op, Glyph, GlyphHash, Metrics,
    Segment,
};

pub struct GlyphBuilder {
    pub parent: Glyph,
    pub child: Glyph,
    pub rng: fastrand::Rng,
}

impl GlyphBuilder {
    pub fn from_glyph(glyph: Glyph) -> Self {
        GlyphBuilder {
            parent: glyph.clone(),
            child: glyph,
            rng: fastrand::Rng::with_seed(0),
        }
    }

    /// Returned mutation must not be applied to a different glyph
    pub fn mutate_child(&mut self) -> Option<Mutation> {
        let mutation = Mutation {
            seed: self.rng.u32(..),
            version: MutationVersion::V0,
            parent_hash: self.parent.to_hash(),
        };
        self.apply_mutation(&mutation)?;
        Some(mutation)
    }

    pub fn apply_mutation(
        &mut self,
        mutation: &Mutation,
    ) -> Option<()> {
        self.child.paths.resize(
            self.parent.paths.len(),
            Default::default(),
        );
        let Mutation {
            seed,
            version: MutationVersion::V0,
            parent_hash,
        } = mutation;
        self.parent.check_hash(*parent_hash);
        let bulge_rng = Rng::with_seed((*seed).into());
        let metrics = Metrics::from_glyphs([&self.parent]);
        for (parent, child) in std::iter::zip(
            &self.parent.paths,
            &mut self.child.paths,
        ) {
            let bulge =
                Bulge::generate(&bulge_rng, metrics);
            child.directions.clear();
            let mut points =
                std::iter::once(parent.first_point)
                    .chain(parent.directions.iter().scan(
                        parent.first_point,
                        |position, &direction| {
                            *position = try_points_op(
                                i16::checked_add,
                                *position,
                                direction.to_point(),
                            )
                            .unwrap();
                            Some(*position)
                        },
                    ))
                    .flat_map(|point| bulge.apply(point));

            let mut position = points.next()?;
            child.first_point = position;
            for point in points {
                let directions =
                    Segment::from_outside_point(
                        try_points_op(
                            i16::checked_sub,
                            point,
                            position,
                        )?,
                    );
                position = point;
                child.directions.extend(directions);
            }
        }

        Some(())
    }

    pub fn swap(&mut self) {
        std::mem::swap(&mut self.parent, &mut self.child);
    }

    pub fn child_glyph(&self) -> &Glyph {
        &self.child
    }
}

#[derive(Encode, Decode, Clone, Debug, PartialEq, Eq)]
pub struct Mutation {
    seed: u32,
    version: MutationVersion,
    parent_hash: GlyphHash,
}

#[derive(Encode, Decode, Clone, Debug, PartialEq, Eq)]
enum MutationVersion {
    V0,
}

#[derive(Encode, Decode, Clone)]
pub struct Bulge {
    center: [i16; 2],
    strength: f32,
    radius: f32,
}

enum DistanceChange {
    Unchanged,
    Changed(f32),
}

// libm is used so that floating point number operations work exactly the same on different platforms

impl Bulge {
    fn generate(rng: &Rng, metrics: Metrics) -> Self {
        // -1..0
        let strength = -rng.f32();
        // 1..(max+1)
        // Value of 0 would cause division error
        let radius = {
            let max = (metrics.rect.x.end
                - metrics.rect.x.start)
                / 4;
            1.0 + (rng.f32() * f32::from(max))
        };
        let x = rng
            .i16(metrics.rect.x.start..metrics.rect.x.end);
        let y = rng
            .i16(metrics.rect.y.start..metrics.rect.y.end);
        Bulge {
            strength,
            radius,
            center: [x, y],
        }
    }
    fn change_distance(
        &self,
        distance: f32,
    ) -> Option<DistanceChange> {
        if distance >= self.radius {
            // * (distance / self.radius) is at least 1, which would make `base` equal 1
            // * `factor` would equal 1 because 1 to the power of anything is 1
            // * anything multiplied by 1 is that thing
            return Some(DistanceChange::Unchanged);
        }
        if distance == 0.0 {
            return None;
        }
        let base = (distance / self.radius).clamp(0.0, 1.0);
        let factor = libm::powf(base, self.strength);
        Some(DistanceChange::Changed(distance * factor))
    }
    fn apply(&self, point: [i16; 2]) -> Option<[i16; 2]> {
        let [x, y] = points_op(
            std::ops::Sub::sub,
            point,
            self.center,
        )
        .map(f32::from);
        match self.change_distance(libm::hypotf(x, y))? {
            DistanceChange::Unchanged => Some(point),
            DistanceChange::Changed(distance) => {
                let angle = libm::atan2f(y, x);
                Some(points_op(
                    std::ops::Add::add,
                    self.center,
                    [libm::cosf(angle), libm::sinf(angle)]
                        .map(|n| {
                            libm::floorf(n * distance)
                                as i16
                        }),
                ))
            }
        }
    }
}

impl std::borrow::Borrow<Glyph> for GlyphBuilder {
    fn borrow(&self) -> &Glyph {
        &self.child
    }
}

impl Default for GlyphBuilder {
    fn default() -> Self {
        GlyphBuilder::from_glyph(Glyph::default())
    }
}

#[test]
fn test_glyph_builder() {
    let mut glyph_builder = GlyphBuilder {
        rng: Rng::with_seed(0),
        parent: Glyph {
            paths: vec![
                workspace_glyph::Path {
                    first_point: [0, 1],
                    directions: vec![
                        Segment([4, 5]),
                        Segment([100, 100]),
                    ],
                };
                3
            ],
        },
        child: Glyph { paths: vec![] },
    };
    let mutation = glyph_builder.mutate_child().unwrap();
    let first = (
        glyph_builder.child.clone(),
        glyph_builder.child.to_hash(),
    );
    for _ in 0..2 {
        glyph_builder.apply_mutation(&mutation);
        assert_eq!(
            first,
            (
                glyph_builder.child.clone(),
                glyph_builder.child.to_hash()
            )
        );
    }
}
