use std::{collections::BTreeMap, num::NonZeroU16};

use crate::database::{Id, Relative};
use bincode::{Decode, Encode};
use workspace_glyph::Glyph;
use workspace_glyph_builder::Mutation;

#[derive(Encode, Decode)]

pub struct Font {
    /// This list's length is the number of glyphs.
    pub glyph_data: BTreeMap<char, GlyphData>,
}

#[derive(Encode, Decode)]

pub struct GlyphData {
    pub first_generation: Id<Generation>,
    pub current_generation: Id<Generation>,
}

#[derive(Encode, Decode)]

pub struct Generation {
    pub glyph: Id<Glyph>,
    /// An ID that might not be in use yet
    pub next: Relative<Id<Generation>>,
    /// Set to None if this is the first generation
    pub non_first_generation_data:
        Option<NonFirstGenerationData>,
}

#[derive(Encode, Decode)]
pub struct NonFirstGenerationData {
    pub score: Score,
    pub origin: Origin,
}

#[derive(Encode, Decode, Clone)]
pub struct Origin {
    pub parent: Id<Generation>,
    pub mutation: Mutation,
}

/*#[derive(DekuRead, DekuWrite)]

pub struct GenerationKey {
    pub font: Id<Font>,
    pub glyph_index: u32,
    pub id: Id<Generation>,
}*/

#[derive(Encode, Decode, Copy, Clone, Debug)]
pub struct Score {
    pub experimental_milliseconds: NonZeroU16,
    pub control_milliseconds: NonZeroU16,
}

impl Default for Score {
    fn default() -> Self {
        Score {
            experimental_milliseconds: 1
                .try_into()
                .unwrap(),
            control_milliseconds: 1.try_into().unwrap(),
        }
    }
}

impl Score {
    pub fn divide(self) -> f32 {
        (self.experimental_milliseconds.get() as f32)
            / (self.control_milliseconds.get() as f32)
    }
}

#[derive(Encode, Decode)]
pub struct License {
    pub license: Vec<u8>,
}
