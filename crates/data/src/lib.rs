pub mod database;
pub mod font;

use std::collections::BTreeMap;

use crate::database::Id;
use bincode::{error::DecodeError, Decode, Encode};
use font::{Font, Origin, Score};
use workspace_glyph::{
    Glyph, GlyphHash, GlyphItem, Metrics,
};
use workspace_glyph_builder::Mutation;

pub const CHARS_PER_TEST: usize = 4;

#[derive(PartialEq, Eq, Debug)]
pub enum Version {
    V0,
    V1,
    V2,
    // Update CURRENT_VERSION when adding a new version
}

pub const CURRENT_VERSION: Version = Version::V2;

impl Encode for Version {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        let byte: u8 = match self {
            Version::V0 => 0x00,
            Version::V1 => 0x01,
            Version::V2 => 0x02,
        };
        byte.encode(encoder)
    }
}

impl Decode for Version {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(match u8::decode(decoder)? {
            0x00 => Version::V0,
            0x01 => Version::V1,
            0x02 => Version::V2,
            byte => {
                return Err(DecodeError::OtherString(
                    format!("Unsupported version: {byte}"),
                ))
            }
        })
    }
}

bincode::impl_borrow_decode!(Version);

#[test]
fn test_version_bincode() {
    assert_eq!(
        bincode::decode_from_slice(
            &[0x02],
            database::bincode_config()
        )
        .unwrap(),
        (CURRENT_VERSION, 1)
    );
    assert_eq!(
        bincode::encode_to_vec(
            CURRENT_VERSION,
            database::bincode_config()
        )
        .unwrap(),
        vec![0x02]
    );
}

pub struct CurrentVersion;

impl Encode for CurrentVersion {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        CURRENT_VERSION.encode(encoder)
    }
}

pub const WRONG_VERSION_ERR: &str = "wrong version";

impl Decode for CurrentVersion {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, DecodeError> {
        let v = Version::decode(decoder)?;
        if v == CURRENT_VERSION {
            Ok(CurrentVersion)
        } else {
            Err(DecodeError::Other(WRONG_VERSION_ERR))
        }
    }
}

bincode::impl_borrow_decode!(CurrentVersion);

#[derive(Encode, Decode)]
pub struct Request {
    pub version: CurrentVersion,
    pub msg: RequestMsg,
}

#[derive(Encode, Decode)]

pub enum RequestMsg {
    UploadGlyphs(Box<UploadGlyphs>),

    StartFirstTest {
        font_id: Id<Font>,
    },

    TestDone {
        font_id: Id<Font>,
        scores: Vec<(GlyphCandidate, Score)>,
        cached_glyph_ids: Vec<Id<Glyph>>,
    },
}

#[derive(Encode, Decode)]
pub struct UploadGlyphs {
    pub password: Password,
    pub license: Vec<u8>,
    pub glyphs: Vec<GlyphItem>,
    /// Included to ensure that the client and server have the same mutation behavior
    pub mutation_hashes:
        BTreeMap<char, (GlyphHash, Mutation)>,
}

#[derive(Encode, Decode)]
pub enum Response {
    WrongVersion,
    Ok(ResponseMsg),
    Other,
}

#[test]
fn test_wrong_version() {
    // Encoding of WrongVersion must not change in future versions
    assert_eq!(
        bincode::encode_to_vec(
            Response::WrongVersion,
            database::bincode_config()
        )
        .unwrap(),
        vec![0x00]
    );
}

impl Response {
    pub fn err(err: impl std::fmt::Debug) -> Self {
        eprintln!("{:?}", err);
        Response::Other
    }
}

#[derive(Encode, Decode)]

pub enum ResponseMsg {
    FontUploaded {
        font_id: Id<Font>,
    },
    WrongPassword,
    TestStarted {
        metrics: Metrics,
        mutated_glyphs: Vec<(GlyphCandidate, ParentGlyph)>,
        /// Sent when responding to RequestMsg::StartFirstTest
        init_data: Option<InitData>,
    },
}

#[derive(Encode, Decode)]
pub enum ParentGlyph {
    Cached(Id<Glyph>, GlyphHash),
    New(Id<Glyph>, Glyph),
}

#[derive(Encode, Decode)]
pub struct InitData {
    pub first_glyphs: Vec<GlyphItem>,
    pub license: String,
}

#[derive(Encode, Decode, Clone)]
pub struct GlyphCandidate {
    pub char: char,
    pub mutated_glyph_id: Id<Glyph>,
    pub origin: Origin,
}

#[derive(PartialEq, Eq, Clone, Encode, Decode)]
pub struct Password(pub [u8; 64]);

impl Password {
    pub fn correct() -> Self {
        Password(password_env().unwrap_or(*b"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
    }
}

fn password_env() -> Option<[u8; 64]> {
    std::env::var("CORRECT_PASSWORD").ok().and_then(|s| {
        <[u8; 64]>::try_from(s.as_bytes()).ok()
    })
}

impl std::str::FromStr for Password {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Password(
            <[u8; 64]>::try_from(s.as_bytes())
                .map_err(|_| ())?,
        ))
    }
}

pub fn api_url() -> Option<&'static str> {
    option_env!("API_URL")
}
