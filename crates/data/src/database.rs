// Functions are async for future-proofing

use anyhow::Result;
use bincode::{
    config::{BigEndian, Fixint},
    error::{DecodeError, EncodeError},
    Decode, Encode,
};
use sled::transaction::{
    ConflictableTransactionError,
    ConflictableTransactionResult,
};
use std::{hash::Hash, marker::PhantomData, ops::BitXor};
use workspace_shared::impl_clone;

#[doc(hidden)]
pub use sled;

pub trait DekuRW
where
    Self: Encode + Decode + Sized,
{
    fn read(
        bytes: &[u8],
        config: impl bincode::config::Config,
    ) -> Result<Self, DecodeError> {
        Ok(bincode::decode_from_slice(bytes, config)?.0)
    }

    fn to_bytes(
        &self,
        config: impl bincode::config::Config,
    ) -> Result<Vec<u8>, EncodeError> {
        bincode::encode_to_vec(self, config)
    }
}

impl<T> DekuRW for T where T: Encode + Decode + Sized {}

pub fn bincode_config() -> bincode::config::Configuration {
    bincode::config::standard()
}

pub fn bincode_key_config(
) -> bincode::config::Configuration<BigEndian, Fixint> {
    bincode::config::standard()
        .with_big_endian()
        .with_fixed_int_encoding()
}

/// Manages all stored data.
#[derive(Clone)]
pub struct Database {
    db: sled::Db,
}

/// Stores items similarly to `std::collections::BTreeMap<Key, T>`.
pub struct Tree<T, Key = Id<T>> {
    tree: sled::Tree,
    phantom: PhantomData<(T, Key)>,
}

#[derive(Encode, Decode, Debug)]
pub struct Id<T> {
    pub id: u64,

    pub phantom: PhantomData<T>,
}

impl<T> Id<T> {
    pub fn relative_to(
        self,
        other: Self,
    ) -> Relative<Self> {
        Relative(Id {
            id: other.id.bitxor(self.id),
            phantom: PhantomData,
        })
    }
}

// Implement traits for Id<T> without bounds on T

impl<T> PartialEq for Id<T> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<T> Eq for Id<T> {}

impl<T> PartialOrd for Id<T> {
    fn partial_cmp(
        &self,
        other: &Self,
    ) -> Option<std::cmp::Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl<T> Ord for Id<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.id.cmp(&other.id)
    }
}

impl<T> Hash for Id<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state)
    }
}

#[test]
fn test_id_bincode() {
    let id = Id::<()> {
        id: 0x000000000000FE00,
        phantom: PhantomData,
    };
    let bytes = vec![0, 0, 0, 0, 0, 0, 0xFE, 0];
    assert_eq!(
        bincode::encode_to_vec(id, bincode_key_config())
            .unwrap(),
        bytes
    );
    assert_eq!(
        bincode::decode_from_slice(
            &bytes,
            bincode_key_config()
        )
        .unwrap(),
        (id, 8)
    );
}

impl<T> std::fmt::Display for Id<T> {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        std::fmt::LowerHex::fmt(&self.id, f)
    }
}

impl<T> std::str::FromStr for Id<T> {
    type Err = std::num::ParseIntError;

    fn from_str(
        src: &str,
    ) -> std::result::Result<Self, Self::Err> {
        Ok(Id {
            id: u64::from_str_radix(src, 16)?,
            phantom: PhantomData,
        })
    }
}

impl_clone!(Tree < T, Key > { tree });
impl_clone!(Id<T> { id });
impl<T> Copy for Id<T> {}

/// Relative<Id<T>> stores the difference between 2 ids instead of a full id, so it's likely to use just 1 byte when serialized with bincode
#[derive(Encode, Decode, Clone, Copy)]
pub struct Relative<T>(T);

impl<T> Relative<Id<T>> {
    pub fn to_absolute(self, other: Id<T>) -> Id<T> {
        Id {
            id: other.id.bitxor(self.0.id),
            phantom: PhantomData,
        }
    }
}

#[test]
fn test_relative_id() {
    let older = Id::<()> {
        id: 0b0100,
        phantom: PhantomData,
    };
    let newer = Id::<()> {
        id: 0b1001,
        phantom: PhantomData,
    };
    let older_rel = older.relative_to(newer);
    assert_eq!(older_rel.0.id, 0b1101);
    assert_eq!(older_rel.to_absolute(newer), older);
}

impl Database {
    /// Open the DullBananasFontGenData directory, which contains all trees.
    pub async fn open() -> Result<Self> {
        Ok(Database {
            db: sled::Config::default()
                .path("DullBananasFontGenData")
                .mode(sled::Mode::LowSpace)
                .open()?,
        })
    }

    /// Open the named tree. `T` must be specified.
    pub async fn tree<T, Key>(
        &self,
        name: &'static [u8],
    ) -> Result<Tree<T, Key>> {
        Ok(Tree {
            tree: self.db.open_tree(name)?,
            phantom: PhantomData,
        })
    }
}

pub struct TransactionalTree<T, Key> {
    tree: sled::transaction::TransactionalTree,
    phantom: PhantomData<(T, Key)>,
}

pub type TResult<T> =
    ConflictableTransactionResult<T, anyhow::Error>;

pub fn t_err<E: Into<anyhow::Error>>(
    err: E,
) -> ConflictableTransactionError<anyhow::Error> {
    ConflictableTransactionError::Abort(err.into())
}

impl<T, Key> Tree<T, Key> {
    #[doc(hidden)]
    pub fn __tree(&self) -> &sled::Tree {
        &self.tree
    }
}

impl<T, Key> Tree<T, Key>
where
    T: DekuRW,
    Key: DekuRW,
{
    pub fn iter(
        &self,
    ) -> impl Iterator<Item = Result<(Key, T)>> {
        self.tree.iter().map(|result| {
            let (k, v) = result?;
            Ok((
                DekuRW::read(&k, bincode_key_config())?,
                DekuRW::read(&v, bincode_config())?,
            ))
        })
    }
}

impl<T, Key> TransactionalTree<T, Key>
where
    T: DekuRW,
    Key: DekuRW,
{
    #[doc(hidden)]
    pub fn __new(
        tree: sled::transaction::TransactionalTree,
        _tree2: &Tree<T, Key>,
    ) -> Self {
        // tree2 argument allows type inference in transaction macro
        TransactionalTree {
            tree,
            phantom: PhantomData,
        }
    }

    /// Insert a value with the specified key.
    pub fn insert_with_key(
        &self,
        key: Key,
        value: &T,
    ) -> TResult<()> {
        self.tree.insert(
            key.to_bytes(bincode_key_config())
                .map_err(t_err)?,
            value
                .to_bytes(bincode_config())
                .map_err(t_err)?,
        )?;
        Ok(())
    }

    /// Return the item's value, or `Ok(None)` if it doesn't exist.
    pub fn get_option(
        &self,
        key: Key,
    ) -> TResult<Option<T>> {
        Ok(
            match self.tree.get(
                key.to_bytes(bincode_key_config())
                    .map_err(t_err)?,
            )? {
                Some(bytes) => Some(
                    DekuRW::read(&bytes, bincode_config())
                        .map_err(t_err)?,
                ),
                None => None,
            },
        )
    }

    /// Return the item's value, or `Err` if it doesn't exist.
    pub fn get(&self, key: Key) -> TResult<T> {
        self.get_option(key)?.ok_or_else(|| {
            t_err(anyhow::format_err!(
                "Failed to get {} value from database",
                std::any::type_name::<T>()
            ))
        })
    }

    pub fn remove(&self, key: Key) -> TResult<Option<T>> {
        Ok(
            match self.tree.remove(
                key.to_bytes(bincode_key_config())
                    .map_err(t_err)?,
            )? {
                Some(bytes) => Some(
                    DekuRW::read(&bytes, bincode_config())
                        .map_err(t_err)?,
                ),
                None => None,
            },
        )
    }

    /*/// Return all items with a key that begins with the specified value.
    pub fn scan_prefix<P>(&self, prefix: P) -> Result<impl Stream<Item = Result<(Key, T), E>>, E>
    where
        P: DekuRW,
    {
        Ok(stream::from_iter(self.tree.scan_prefix(prefix.to_bytes()?)
            .map(|result| match result {
                Ok((key, value)) => Ok((
                    DekuRW::read(&key)?,
                    DekuRW::read(&value)?,
                )),
                Err(err) => Err(err.into()),
            })
        ))
    }*/
}

impl<T> TransactionalTree<T, Id<T>>
where
    T: DekuRW,
{
    pub fn generate_id(&self) -> TResult<Id<T>> {
        Ok(Id {
            id: self.tree.generate_id()?,
            phantom: PhantomData,
        })
    }
    /// Insert a value with an automatically chosen key that hasn't been used yet, and return the key.
    pub fn insert(&self, value: &T) -> TResult<Id<T>> {
        let key = self.generate_id()?;
        self.insert_with_key(key, value)?;
        Ok(key)
    }
}

pub type TransactionResult<T> =
    sled::transaction::ConflictableTransactionResult<
        T,
        anyhow::Error,
    >;

#[macro_export]
macro_rules! transaction {
    (
        ($($tree:expr),+),
        |$($arg:ident),+|
        $body:tt
    ) => {{
        //let f = |$($arg),+| $body;
        let trees = ($(($tree).__tree()),+);
        let result = $crate::database::sled::transaction::Transactional::transaction(&trees, move |($($arg),+)| {
            $(let $arg = $crate::database::TransactionalTree::__new($arg.clone(), $tree);)+
            $body
        }).map_err(|err| match err {
            $crate::database::sled::transaction::TransactionError::Abort(x)=>x,
            $crate::database::sled::transaction::TransactionError::Storage(x)=>::anyhow::Error::from(x),
        });
        async {
            result
        }
    }};
}
