mod state;

use crate::state::State;
use anyhow::{Context, Result};
use axum::body::Bytes;
#[cfg(feature = "static-files")]
use axum::http::{HeaderMap, HeaderValue, StatusCode, Uri};
use axum::response::IntoResponse;
use axum::routing::{get, post};
use axum::{Router, Server};
use bincode::error::DecodeError;
use std::net::SocketAddr;
use std::time::Duration;
use tower_http::cors::CorsLayer;
use workspace_data as api;
use workspace_data::database::bincode_config;

fn main() -> Result<()> {
    #[cfg(feature = "full")]
    let mut runtime =
        tokio::runtime::Builder::new_multi_thread();
    #[cfg(not(feature = "full"))]
    let mut runtime =
        tokio::runtime::Builder::new_current_thread();

    runtime
        .enable_io()
        .enable_time()
        .build()?
        .block_on(run_server())?;

    Ok(())
}

async fn run_server() -> Result<()> {
    println!(
        "Password: {}",
        std::str::from_utf8(&api::Password::correct().0)?
    );
    let state =
        State::new().await.context("State::new failed")?;

    let router = Router::new()
        .route("/*path", get(serve_file))
        .route("/", get(serve_file))
        .route("/", post(api_respond))
        .route(
            "/v",
            get(|| async {
                format!("{:?}", api::CURRENT_VERSION)
            }),
        )
        .with_state(state)
        .layer(
            CorsLayer::very_permissive()
                .max_age(Duration::from_secs(24 * 3600)),
        )
        // TODO: 10mb limit
        .layer(axum::extract::DefaultBodyLimit::disable());

    let server = Server::try_bind(&get_address()?)?
        .serve(router.into_make_service());

    println!(
        "Server is now running at http://{}\nPress enter to stop",
        server.local_addr()
    );

    let (tx, rx) = tokio::sync::oneshot::channel();
    let handle =
        std::thread::spawn(|| -> std::io::Result<()> {
            // Wait for input
            if let Some(result) =
                std::io::Read::bytes(std::io::stdin())
                    .next()
            {
                result?;
                tx.send(()).unwrap();
            }
            Ok(())
        });
    server
        .with_graceful_shutdown(async {
            rx.await.unwrap();
        })
        .await?;
    handle.join().unwrap()?;

    Ok(())
}

fn get_address() -> Result<SocketAddr> {
    match std::env::var("ADDRESS") {
        Ok(var_value) => Ok(var_value.parse().context(
            "Failed to parse ADDRESS environment variable",
        )?),
        Err(std::env::VarError::NotPresent) => {
            println!("Port will be automatically selected. Server address can be manually set using the ADDRESS environment variable.");
            Ok(([127, 0, 0, 1], 0).into())
        }
        Err(error) => Err(error.into()),
    }
}

#[cfg(not(feature = "static-files"))]
async fn serve_file() -> impl IntoResponse {
    ""
}

#[cfg(feature = "static-files")]
async fn serve_file(
    state: axum::extract::State<State>,
    uri: Uri,
) -> impl IntoResponse {
    let path = uri.path().trim_start_matches('/');

    let mut headers = HeaderMap::new();
    headers.insert(
        "Content-Type",
        HeaderValue::from_static("text/html"),
    );

    if path.ends_with(".html") || path.is_empty() {
        (
            StatusCode::OK,
            headers,
            state.static_files.index_html.clone(),
        )
    } else if let Some((mime_type, bytes)) =
        state.static_files.files.get(path.as_bytes())
    {
        headers.insert(
            "Content-Type",
            HeaderValue::from_static(mime_type),
        );
        (StatusCode::OK, headers, bytes.clone())
    } else {
        (
            StatusCode::NOT_FOUND,
            headers,
            b"404 Not Found".to_vec(),
        )
    }
}

async fn api_respond(
    state: axum::extract::State<State>,
    input: Bytes,
) -> impl IntoResponse {
    let request: Result<api::Request, _> =
        bincode::decode_from_slice(
            &input,
            bincode_config(),
        )
        .map(|(x, _)| x);
    let response = match request {
        Ok(api::Request { version: _, msg }) => {
            match state.0.handle_request(msg).await {
                Ok(response_msg) => {
                    api::Response::Ok(response_msg)
                }
                Err(err) => api::Response::err(err),
            }
        }
        Err(DecodeError::Other(api::WRONG_VERSION_ERR)) => {
            api::Response::WrongVersion
        }
        Err(err) => api::Response::err(
            anyhow::Error::new(err)
                .context("Failed to parse request body"),
        ),
    };
    bincode::encode_to_vec(response, bincode_config())
        .unwrap()
}
