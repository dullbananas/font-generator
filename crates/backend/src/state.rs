// TODO: database operations should be atomic
use anyhow::{Context, Result};
use api::database::t_err;
use api::font::{
    Font, Generation, GlyphData, License,
    NonFirstGenerationData, Origin, Score,
};
use api::{
    transaction, GlyphCandidate, ParentGlyph, RequestMsg,
    ResponseMsg,
};
use fine_ill_do_it_myself::DirSizeTracker;
use std::collections::BTreeMap;
use std::time::{Duration, SystemTime};
use workspace_data as api;
use workspace_data::database::{Database, Id, Tree};
use workspace_glyph::{Glyph, GlyphItem, Metrics};
use workspace_glyph_builder::GlyphBuilder;

const MIN_GLYPH_DURATION: Duration =
    Duration::from_secs(60 * 60);

#[derive(Clone)]
pub struct State {
    fonts: Tree<Font>,
    glyph_generations: Tree<Generation>,
    glyphs: Tree<Glyph>,
    /// Stores the creation times of candidate glyphs so they can be deleted after expiration
    glyph_times: Tree<SystemTime, Id<Glyph>>,
    licenses: Tree<License, Id<Font>>,
    size_limit: Option<SizeLimit>,
    #[cfg(feature = "static-files")]
    pub static_files: std::sync::Arc<StaticFiles>,
}

/// Limit is specified by SIZE_LIMIT environment variable with path:bytes format
#[derive(Clone)]
struct SizeLimit {
    tracker: DirSizeTracker,
    max_bytes: u64,
}

impl SizeLimit {
    fn new(env_value: String) -> Result<Self> {
        let max_bytes = env_value.split_once(':').context("SIZE_LIMIT must specify maximum bytes after colon")?.1.parse().context("Invalid number of maximum bytes")?;
        let path = {
            let mut env_value = env_value;
            env_value
                .truncate(env_value.find(':').unwrap());
            env_value
        };
        println!(
            "Size limit set to {max_bytes} bytes at {path}"
        );
        let tracker = DirSizeTracker::new(
            path.into(),
            fine_ill_do_it_myself::Mode::Metadata,
        )?;
        Ok(SizeLimit { tracker, max_bytes })
    }
}

#[cfg(feature = "static-files")]
pub struct StaticFiles {
    pub index_html: Vec<u8>,
    pub files: std::collections::HashMap<
        &'static [u8],
        (&'static str, Vec<u8>),
    >,
}

impl State {
    pub async fn new() -> Result<Self> {
        let db = Database::open().await?;

        let version_tree: Tree<api::Version, ()> =
            db.tree(b"version").await?;
        let version_tree = &version_tree;
        transaction!((version_tree), |t| {
            match t.get_option(())? {
                // Migration code will go here
                Some(
                    version @ (api::Version::V0 | api::Version::V1),
                ) => {
                    panic!("Migration from {version:?} won't be implemented")
                }
                Some(api::CURRENT_VERSION) | None => {}
            }
            t.insert_with_key((), &(api::CURRENT_VERSION))?;
            Ok(())
        })
        .await?;

        let size_limit = std::env::var("SIZE_LIMIT")
            .ok()
            .map(SizeLimit::new)
            .transpose()
            .context("Failed to create size limit")?;

        if size_limit.is_none() {
            println!("Size limit not set");
        }

        let state = State {
            fonts: db.tree(b"fonts").await?,
            glyph_generations: db.tree(b"glyph_generations").await?,
            glyphs: db.tree(b"glyphs").await?,
            glyph_times: db.tree(b"glyph_times").await?,
            licenses: db.tree(b"license").await?,
            size_limit,
            #[cfg(feature = "static-files")]
            static_files: std::sync::Arc::new(StaticFiles {
                index_html: std::fs::read("index.html")?,
                files: [
                    ("text/css", "style.css"),
                    ("text/javascript", "target/wasm.js"),
                    ("application/wasm", "target/wasm_bg.wasm"),
                ]
                .into_iter()
                .map(|(mime_type, path)| {
                    anyhow::Ok((
                        path.as_bytes(),
                        (
                            mime_type,
                            std::fs::read(format!(
                                "crates/frontend-cdylib/static/{path}"
                            ))?,
                        ),
                    ))
                })
                .collect::<Result<std::collections::HashMap<_, _>, _>>()
                .context("Failed to load static files")?,
            }),
        };

        /*let state2 = state.clone();

        tokio::spawn(async move {
            let mut interval =
                tokio::time::interval(MIN_GLYPH_DURATION);
            interval.set_missed_tick_behavior(
                MissedTickBehavior::Skip,
            );
            loop {
                interval.tick().await;
                if let Err(err) = state2.cleanup().await {
                    println!("{:?}", err);
                } else {
                    println!("Cleanup done");
                }
            }
        });*/

        Ok(state)
    }

    async fn cleanup(&self) -> Result<()> {
        let expired_glyph_ids = self
            .glyph_times
            .iter()
            .filter_map(|result| match result {
                Ok((id, time))
                    if time.elapsed().ok()?
                        > MIN_GLYPH_DURATION =>
                {
                    Some(Ok(id))
                }
                Ok(_) => None,
                Err(err) => Some(Err(err)),
            })
            .collect::<Result<Vec<_>>>()?;
        transaction!(
            (&self.glyphs, &self.glyph_times),
            |glyphs, glyph_times| {
                for id in &expired_glyph_ids {
                    glyphs.remove(*id)?;
                    glyph_times.remove(*id)?;
                }
                Ok(())
            }
        )
        .await?;
        Ok(())
    }

    pub async fn handle_request(
        &self,
        msg: RequestMsg,
    ) -> Result<api::ResponseMsg> {
        if let Some(SizeLimit { tracker, max_bytes }) =
            self.size_limit.as_ref()
        {
            let size = tracker.get_total_size()?;
            if size > *max_bytes {
                return Err(anyhow::anyhow!("size of {size} bytes exceeds limit of {max_bytes} bytes"));
            }
        }
        match msg {
            RequestMsg::UploadGlyphs(boxed) => {
                let api::UploadGlyphs {
                    password,
                    license,
                    glyphs,
                    mutation_hashes,
                } = *boxed;
                if password == api::Password::correct() {
                    for i in glyphs.iter() {
                        if let Some((hash, mutation)) =
                            mutation_hashes.get(&i.char)
                        {
                            let mut glyph_builder =
                                GlyphBuilder::from_glyph(
                                    i.glyph.clone(),
                                );
                            assert_eq!(
                                mutation,
                                &glyph_builder
                                    .mutate_child()
                                    .unwrap()
                            );
                            glyph_builder
                                .child
                                .check_hash(*hash);
                        }
                    }
                    let font_id = self
                        .add_font(glyphs, license)
                        .await?;
                    Ok(ResponseMsg::FontUploaded {
                        font_id,
                    })
                } else {
                    Ok(ResponseMsg::WrongPassword)
                }
            }
            RequestMsg::StartFirstTest { font_id } => {
                let (metrics, mutated_glyphs) =
                    self.start_test(font_id, &[]).await?;
                let first_glyphs =
                    self.get_first_glyphs(font_id).await?;
                let license_value = transaction!(
                    (&self.licenses),
                    |licenses| {
                        let value =
                            licenses.get(font_id)?;
                        Ok(value)
                    }
                )
                .await?;
                let license = String::from_utf8(
                    license_value.license,
                )?;
                let init_data = Some(api::InitData {
                    first_glyphs,
                    license,
                });
                Ok(ResponseMsg::TestStarted {
                    metrics,
                    mutated_glyphs,
                    init_data,
                })
            }
            RequestMsg::TestDone {
                font_id,
                scores,
                cached_glyph_ids,
            } => {
                self.submit_time(font_id, scores).await?;
                let (metrics, mutated_glyphs) = self
                    .start_test(font_id, &cached_glyph_ids)
                    .await?;
                Ok(ResponseMsg::TestStarted {
                    metrics,
                    mutated_glyphs,
                    init_data: None,
                })
            }
        }
    }

    async fn add_font(
        &self,
        new_glyphs: Vec<GlyphItem>,
        license: Vec<u8>,
    ) -> Result<Id<Font>> {
        let font_id = transaction!(
            (
                &self.fonts,
                &self.glyphs,
                &self.glyph_generations,
                &self.licenses
            ),
            |fonts, glyphs, glyph_generations, licenses| {
                let mut glyph_data =
                    BTreeMap::<char, GlyphData>::new();

                for GlyphItem { char, glyph, .. } in
                    &new_glyphs
                {
                    // `first_generation_id` must be generated before the next generation's ID so they are sorted
                    let first_generation_id =
                        glyph_generations.generate_id()?;

                    glyph_generations.insert_with_key(
                        first_generation_id,
                        &Generation {
                            glyph: glyphs.insert(glyph)?,
                            next: glyph_generations
                                .generate_id()?
                                .relative_to(
                                    first_generation_id,
                                ),
                            non_first_generation_data: None,
                        },
                    )?;

                    glyph_data.insert(
                        *char,
                        GlyphData {
                            first_generation:
                                first_generation_id,
                            current_generation:
                                first_generation_id,
                        },
                    );
                }

                let font_id =
                    fonts.insert(&Font { glyph_data })?;
                licenses.insert_with_key(
                    font_id,
                    &License {
                        license: license.clone(),
                    },
                )?;
                Ok(font_id)
            }
        )
        .await?;
        Ok(font_id)
    }

    async fn submit_time(
        &self,
        font_id: Id<Font>,
        scores: Vec<(GlyphCandidate, Score)>,
    ) -> Result<()> {
        transaction!((&self.fonts, &self.glyph_generations, &self.glyphs, &self.glyph_times), |fonts, glyph_generations, glyphs, glyph_times| {
        let Some(mut font) = fonts.get_option(font_id)? else {
            // Do nothing if font was deleted
            return Ok(());
        };

        for (
            GlyphCandidate {
                char,
                mutated_glyph_id,
                origin,
            },
            new_score,
        ) in &scores
        {
            // Don't use expired glyphs
            if glyphs.get_option(*mutated_glyph_id)?.is_none() {
                continue;
            }
            let mut glyph_data = font
                .glyph_data
                .get_mut(char)
                .context("char out of bounds").map_err(t_err)?;

            let generation = glyph_generations
                .get(glyph_data.current_generation)
                ?;

            match generation.non_first_generation_data.map(|x|x.score) {
                Some(score)
                    if new_score.divide()
                        > score.divide() =>
                {
                    // Higher score is worse
                    let _: Option<Glyph> = glyphs
                        .remove(*mutated_glyph_id)?;
                }
                _ => {
                    glyph_times.remove(*mutated_glyph_id)?;
                    let new_generation_id =
                    generation.next.to_absolute(glyph_data.current_generation);
                    glyph_generations
                        .insert_with_key(
                            new_generation_id,
                            &Generation {
                                glyph: *mutated_glyph_id,
                                next: glyph_generations.generate_id()
                                ?.relative_to(new_generation_id),
                                non_first_generation_data: Some(NonFirstGenerationData {
                                    score: *new_score,
                                    origin: origin.clone(),
                                }),
                            },
                        )?;
                    glyph_data.current_generation =
                        new_generation_id;
                    fonts
                        .insert_with_key(font_id, &font)
                        ?;
                }
            };
        }

        Ok(())}).await?;
        Ok(())
    }

    async fn start_test(
        &self,
        font_id: Id<Font>,
        cached_glyph_ids: &[Id<Glyph>],
    ) -> Result<(Metrics, Vec<(GlyphCandidate, ParentGlyph)>)>
    {
        self.cleanup().await?;
        let result = transaction!(
            (
                &self.fonts,
                &self.glyph_generations,
                &self.glyphs,
                &self.glyph_times
            ),
            |fonts,
             glyph_generations,
             glyphs,
             glyph_times| {
                let glyph_data = {
                    let mut glyph_data = fonts
                        .get(font_id)?
                        .glyph_data
                        .into_iter()
                        .filter(|i| char_is_testable(i.0))
                        .collect::<Vec<_>>();
                    fastrand::shuffle(&mut glyph_data);
                    glyph_data.truncate(
                        workspace_data::CHARS_PER_TEST,
                    );
                    glyph_data
                };
                let mut glyph_builder = GlyphBuilder {
                    rng: fastrand::Rng::with_seed(
                        fastrand::get_seed(),
                    ),
                    ..Default::default()
                };
                let mut result =
                    Vec::with_capacity(glyph_data.len());
                let mut metrics_glyphs =
                    Vec::with_capacity(glyph_data.len());
                for (
                    char,
                    GlyphData {
                        current_generation, ..
                    },
                ) in glyph_data
                {
                    let generation = glyph_generations
                        .get(current_generation)?;
                    glyph_builder.parent =
                        glyphs.get(generation.glyph)?;
                    let option_parent_glyph =
                        if cached_glyph_ids
                            .contains(&generation.glyph)
                        {
                            ParentGlyph::Cached(
                                generation.glyph,
                                glyph_builder
                                    .parent
                                    .to_hash(),
                            )
                        } else {
                            ParentGlyph::New(
                                generation.glyph,
                                glyph_builder
                                    .parent
                                    .clone(),
                            )
                        };
                    let mutation = glyph_builder
                        .mutate_child()
                        .context("Failed to mutate glyph")
                        .map_err(t_err)?;
                    let mutated_glyph_id = glyphs
                        .insert(&glyph_builder.child)?;
                    glyph_times.insert_with_key(
                        mutated_glyph_id,
                        &SystemTime::now(),
                    )?;
                    let origin = Origin {
                        parent: current_generation,
                        mutation,
                    };
                    result.push((
                        GlyphCandidate {
                            char,
                            mutated_glyph_id,
                            origin,
                        },
                        option_parent_glyph,
                    ));
                    metrics_glyphs
                        .push(glyph_builder.child.clone());
                }
                fastrand::seed(
                    glyph_builder.rng.get_seed(),
                );
                let metrics =
                    Metrics::from_glyphs(&metrics_glyphs);
                Ok((metrics, result))
            }
        )
        .await?;
        Ok(result)
    }

    async fn get_first_glyphs(
        &self,
        font_id: Id<Font>,
    ) -> Result<Vec<GlyphItem>> {
        let glyphs = transaction!(
            (
                &self.fonts,
                &self.glyph_generations,
                &self.glyphs
            ),
            |fonts, glyph_generations, glyphs| {
                let font = fonts.get(font_id)?;
                let mut glyphs_vec = Vec::with_capacity(
                    font.glyph_data.len(),
                );
                for (char, glyph_data) in
                    font.glyph_data.into_iter()
                {
                    if char_is_testable(char) {
                        let generation = glyph_generations
                            .get(
                                glyph_data.first_generation,
                            )?;
                        let glyph =
                            glyphs.get(generation.glyph)?;
                        glyphs_vec.push(GlyphItem {
                            char,
                            glyph,
                        });
                    }
                }
                Ok(glyphs_vec)
            }
        )
        .await?;
        Ok(glyphs)
    }

    /*pub async fn get_test_glyph(
        &self,
        user_id: Id<User>,
    ) -> Result<Option<Glyph>, E> {
        Ok(match self.active_tests.get_option(user_id).await? {
            Some(test) => self.glyphs.get_option(test.glyph).await?,
            None => None,
        })
    }

    async fn get_glyph_char(&self, glyph_id: Id<Glyph>) -> Result<char, E> {
        Ok(self.glyphs.get(glyph_id).await?.char)
    }*/
}

fn char_is_testable(char: char) -> bool {
    char.is_ascii_lowercase()
}
