#![cfg(test)]

/*#[test]
fn glyph_bytes_conversion() -> Result<(), deku::error::DekuError> {
    let bytes: &'static [u8] = &[
        // number of points in path 0, excluding `first_point`
        0, 3,
        // point 0
        64, 0, // x
        32, 0, // y
        0, 0, 0, 0, // radians
        16, 0, // curviness
        // point 1
        96, 0, // x
        64, 0, // y
        63, 201, 15, 219, // radians
        16, 0, // curviness
        // point 2
        32, 0, // x
        64, 0, // y
        64, 150, 203, 228, // radians
        16, 0, // curviness
    ];
    let glyph = Glyph::new('a');
    assert_eq!(
        deku::DekuContainerWrite::to_bytes(&glyph)?,
        bytes,
    );
    assert_eq!(
        <Glyph as deku::DekuContainerRead>::from_bytes((bytes, 0))?.1,
        glyph,
    );

    Ok(())
}*/

/*#[test]
fn test_direction() {
    let points = [
        (0, [1, -1]),
        (1, [0, -1]),
        (2, [-1, -1]),
        (3, [-1, 0]),
        (4, [-1, 1]),
        (5, [0, 1]),
        (6, [1, 1]),
        (7, [1, 0]),
    ];
    for (direction, [x, y]) in points.into_iter() {
        let point = [x * 32, y * 32];
        assert_eq!(
            Direction(direction * 32).to_point(),
            point
        );
        assert_eq!(
            Direction::from_outside_point(point),
            Some(Direction(direction * 32))
        );
        assert_eq!(
            Direction::from_outside_point([x * 64, y * 64]),
            Some(Direction(direction * 32))
        );
        assert_eq!(
            Direction::from_outside_point([x * 31, y * 31]),
            None
        );
    }
}*/
