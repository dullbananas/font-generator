mod tests;

use axgeom::{Range, Rect};
use bincode::{Decode, Encode};
use fxhash::FxHasher32;
use std::{
    borrow::Borrow,
    hash::{Hash, Hasher},
    num::NonZeroU8,
};

#[derive(Copy, Clone)]
pub struct Metrics {
    pub rect: Rect<i16>,
}

impl Default for Metrics {
    fn default() -> Self {
        Metrics {
            rect: Rect::new(0, 0, 0, 0),
        }
    }
}

impl Encode for Metrics {
    fn encode<E: bincode::enc::Encoder>(
        &self,
        encoder: &mut E,
    ) -> Result<(), bincode::error::EncodeError> {
        self.rect.get().encode(encoder)?;
        Ok(())
    }
}

impl Decode for Metrics {
    fn decode<D: bincode::de::Decoder>(
        decoder: &mut D,
    ) -> Result<Self, bincode::error::DecodeError> {
        Ok(Self {
            rect: <[i16; 4]>::decode(decoder)?.into(),
        })
    }
}

bincode::impl_borrow_decode!(Metrics);

impl Metrics {
    pub fn from_glyphs<'a, T: Borrow<Glyph> + 'a>(
        glyphs: impl IntoIterator<Item = &'a T>,
    ) -> Metrics {
        let mut points = glyphs
            .into_iter()
            .flat_map(|glyph| &glyph.borrow().paths)
            .flat_map(|path| {
                let mut directions = path.directions.iter();
                std::iter::successors(
                    Some(path.first_point),
                    move |&point| {
                        Some(points_op(
                            i16::wrapping_add,
                            point,
                            directions.next()?.to_point(),
                        ))
                    },
                )
            });

        let mut metrics = {
            let [x, y] = points.next().unwrap_or([0, 0]);
            Metrics {
                rect: Rect {
                    x: Range { start: x, end: x },
                    y: Range { start: y, end: y },
                },
            }
        };

        for [x, y] in points {
            metrics
                .rect
                .grow_to_fit_point(axgeom::Vec2 { x, y });
        }

        metrics
    }
}

#[derive(Encode, Decode)]
pub struct GlyphItem {
    pub char: char,
    pub glyph: Glyph,
}

#[derive(
    Encode, Decode, Clone, Debug, Default, PartialEq, Eq,
)]
pub struct Glyph {
    pub paths: Vec<Path>,
}

impl Glyph {
    /// Used for detecting bugs that cause incorrect glyphs to be used. Must produce the same result regardless of the platform's endianness or pointer size.
    pub fn to_hash(&self) -> GlyphHash {
        let mut sha = FxHasher32::default();
        sha.write_u32(self.paths.len() as u32);
        for path in &self.paths {
            let [x, y] = path.first_point;
            sha.write_i16(x);
            sha.write_i16(y);
            sha.write_u32(path.directions.len() as u32);
            for &Segment([x, y]) in &path.directions {
                sha.write_i16(x);
                sha.write_i16(y);
            }
        }
        GlyphHash(
            sha.finish().to_le_bytes()[7]
                .try_into()
                .unwrap_or(1.try_into().unwrap()),
        )
    }

    pub fn check_hash(&self, hash: GlyphHash) {
        assert_eq!(self.to_hash(), hash);
    }
}

#[derive(
    Encode, Decode, PartialEq, Eq, Debug, Clone, Copy,
)]
pub struct GlyphHash(NonZeroU8);

#[derive(
    Encode, Decode, Clone, Debug, Default, PartialEq, Eq,
)]

pub struct Path {
    /// This stores coordinates as `[x, y]` starting at the bottom left and incerasing to the top right.
    pub first_point: [i16; 2],

    pub directions: Vec<Segment>,
}

impl Path {
    pub fn close(&mut self) {
        let mut end = [0, 0];
        for direction in self.directions.iter().copied() {
            end = points_op(
                std::ops::Sub::sub,
                end,
                direction.to_point(),
            );
        }
        for direction in Segment::from_outside_point(end) {
            self.directions.push(direction);
            end = points_op(
                std::ops::Sub::sub,
                end,
                direction.to_point(),
            );
        }
    }
}

#[derive(
    Encode, Decode, Copy, Clone, Debug, PartialEq, Eq, Hash,
)]
pub struct Segment(pub [i16; 2]);

impl Segment {
    pub fn to_point(self) -> [i16; 2] {
        self.0
    }

    pub fn from_outside_point(
        p: [i16; 2],
    ) -> impl Iterator<Item = Self> {
        std::iter::once(Segment(p))
    }
}

pub fn points_op(
    mut f: impl FnMut(i16, i16) -> i16,
    a: [i16; 2],
    b: [i16; 2],
) -> [i16; 2] {
    [f(a[0], b[0]), f(a[1], b[1])]
}

pub fn try_points_op(
    mut f: impl FnMut(i16, i16) -> Option<i16>,
    a: [i16; 2],
    b: [i16; 2],
) -> Option<[i16; 2]> {
    Some([f(a[0], b[0])?, f(a[1], b[1])?])
}
