# Font generator

Not working on this a lot anymore

https://font-generator.dullbananas.com/test.html?font=1e9d04

## Build		
		
Requirements:
		
* rust
* wasm-pack 0.11
* bash
		
To compile and run the server with dev profile, run this in a terminal. Address can be changed and is optional.
		
```
ADDRESS=127.0.0.1:8000 sh scripts/dev-server.sh
```

# Deploy

For domcloud, see domcloud.yml

## Usage

1. Go to /editor.html
2. Choose font file with top button
3. Use top text field and "toggle mutation" button to select characters and see what mutation looks like
4. In the long text box, past the `CORRECT_PASSWORD` value set on the server
5. Click upload
